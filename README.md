# Payment Processing System

This Payment Processing System is a Spring Boot application designed to facilitate the processing of payment transactions across various Payment Service Providers (PSPs).

## Table of Contents

- [Features](#features)
- [Getting Started](#getting-started)
    - [Prerequisites](#prerequisites)
    - [Installation](#installation)
- [Usage](#usage)
- [Payment Processing Workflow](#payment-processing-workflow)
- [Validation](#validation)
- [Error Handling](#error-handling)
- [Fraud Detection](#fraud-detection)
- [Payment Load Balancing](#payment-load-balancing)
    - [Round Robin Balancer](#round-robin-balancer)
    - [Monetary Volume Balancer](#monetary-volume-balancer)
- [Payment Service Providers](#payment-service-providers)
- [License](#license)
- [Contact](#contact)
- [Acknowledgments](#acknowledgments)

## Features

- Payment transaction processing with multiple PSPs
- Comprehensive validation of payment requests
- Support for multiple card types and currencies
- Fraud detection based on an amount threshold
- Load balancing strategies to optimize PSP utilization
- Detailed error handling and response messages

## Getting Started

### Prerequisites

- Java 17 or later
- Maven
- Spring Boot
- Docker (optional)

### Installation

1. Clone the repository:

   ``git clone https://gitlab.com/alexandros3001643/assignment.git``

2. Navigate to the project directory:

   ``cd payment-processing-system``

3. Build the project using Maven:

   ``mvn clean install``

4. (Optional) Building Docker Image:

   **Note**: Make sure you have Docker installed on your machine.

   ``mvn compile jib:dockerBuild``
   
   **Note**: If jib is complaining about credentials, ensure you are logged into your Docker account. If you are already logged in and still encountering errors, try logging out and logging back in again to resolve the issue.

5. Run the application:

   ``mvn spring-boot:run``

   or

   ``java -jar target/payment-processing-system-0.0.1-SNAPSHOT.jar``

   or if you have built the Docker image, run it with:

   ``docker run -p 8080:8080 alexandros-assignment``

This will start a web server listening on port 8080.

### Docker Image Creation with Jib

This project utilizes [Jib](https://github.com/GoogleContainerTools/jib) for building Docker images:

- **Fast and Efficient**: Jib optimizes image creation, making it faster and more efficient than traditional Docker builds.
- **No Docker Daemon Required**: Jib builds images directly to a Docker registry without requiring a Docker daemon, streamlining the process.
- **Reproducible Builds**: Images built with Jib are reproducible, helping ensure consistency across different environments.
- **Layering Optimization**: Jib optimally layers the Docker image, improving build, pull and push times.
- **Easy Configuration**: Configuring Jib can be done entirely through Maven or Gradle, simplifying the build process.

Using Jib integrates seamlessly with existing Java build tools, enhancing the development and deployment workflow.


## Usage

To process a payment, send a **POST** request to `http://localhost:8008/api/payment` with a JSON payload representing the `PaymentRequest`. The system will validate the request, route it to the appropriate PSP, and return a `PaymentResponse`.

Once the application is up and running, you can use the following `curl` command as an example to make a payment request:

```bash
curl -X POST http://localhost:8080/api/payment \
-H "Content-Type: application/json" \
-H "Idempotency-Key: your-unique-key" \
-d '{
  "card": {
    "cardNumber": "4111111111111111",
    "expiryDate": "12/25",
    "cvv": "123"
  },
  "amount": 100.00,
  "currency": "USD",
  "merchantId": "merchant123"
}'
```

For Approved transaction you can use this card number:
``4111111111111110``

For Denied transaction you can use this card number:
``4111111111111111``

For both **Approved** and **Denied** transaction the server will return HTTP status code 200.
In case of validation errors, the system returns HTTP status code 400 with, status ``Failed`` and message ``Validation error``.
For more details about the error, check the server logs.

When the same Idempotency-Key and Merchant Id is used for a second time, the server will return the cached response. 
Internally the system uses a cache to store the responses for each Idempotency-Key and Merchant Id combination.

The system will treat the transaction as `fraudulent` when the amount is greater than `10000.00` USD.

## Payment Processing Workflow

1. **Validation**: The system validates the payment request based on predefined rules and annotations.
2. **Fraud Detection**: The system checks for suspicious activity based on an amount threshold.
3. **Routing**: Based on the card type and currency, the system routes the request to the appropriate PSP.
4. **Processing**: The PSP processes the payment and returns a response.

## Validation

The system validates the payment request using custom and built-in annotations, ensuring all fields meet the required criteria.

## Error Handling

In case of validation errors or other issues, the system provides detailed error messages and HTTP status codes to help 
identify the issue. To investigate and resolve the issue the system also logs the error messages.

## Fraud Detection

The system checks for suspicious activity based on an amount threshold. If the amount exceeds the threshold, 
the system returns an error message and denies the transaction.

## Payment Load Balancing

### Round Robin Balancer

Distributes payment requests evenly among all available PSPs.

#### How it Works

1. **Distribution**: On each payment request, it selects the next PSP in the list.
2. **Cycle**: After reaching the last PSP, it cycles back to the first.

### Monetary Volume Balancer

Balances payment requests based on the monetary volume each PSP is currently processing.

#### How it Works

1. **Selection**: For each payment request, it selects the PSP with the lowest current monetary volume for the specific currency.
2. **Update**: The monetary volume of the selected PSP is updated proactively to reflect the incoming transaction.
3. **Revert**: In case of transaction failure, the monetary volume is reverted back.

## Payment Service Providers

Each PSP extends the `PaymentServiceProvider` class and implements specific payment processing logic.

#### Core Responsibilities

1. **Prepare**: Prepares the PSP for processing a payment, updating the monetary volume.
2. **Process**: Processes the payment request and generates a response.
3. **Monetary Volume Management**: Manages the monetary volume for each currency.

#### Transaction Logic

- **Approval**: Transactions with card numbers ending in an even digit are approved.
- **Denial**: Transactions with card numbers ending in an odd digit are denied, and the monetary volume is reverted.

## License

Distributed under the MIT License. See `LICENSE` for more information.

## Contact

Alexandros Ioannou - [ioannoual@gmail.com](mailto:ioannoual@gmail.com)

Project Link: [https://gitlab.com/alexandros3001643/assignment](https://gitlab.com/alexandros3001643/assignment)

## Acknowledgments

- [Spring Boot](https://spring.io/projects/spring-boot)
- [Maven](https://maven.apache.org/)
- [Java](https://www.java.com/)
- [Docker](https://www.docker.com/)
- [Jib](https://github.com/GoogleContainerTools/jib)
- [Lombok](https://projectlombok.org/)