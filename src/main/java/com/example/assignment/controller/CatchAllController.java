package com.example.assignment.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CatchAllController {

    @RequestMapping("/**")
    public ResponseEntity<String> handleUncaughtException() {
        return new ResponseEntity<>("Wubba Lubba Dub Dub! This page is in another dimension!", HttpStatus.NOT_FOUND);
    }
}

