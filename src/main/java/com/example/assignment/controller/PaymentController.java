package com.example.assignment.controller;

import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import com.example.assignment.service.IdempotencyService;
import com.example.assignment.service.PaymentService;
import com.example.assignment.util.SecurityUtil;
import jakarta.validation.Valid;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/payment")
@Log4j2
public class PaymentController {

    private final IdempotencyService idempotencyService;

    private final PaymentService paymentService;

    public PaymentController(IdempotencyService idempotencyService, PaymentService paymentService) {
        this.idempotencyService = idempotencyService;
        this.paymentService = paymentService;
    }

    /**
     * Handles a payment processing request.
     * <p>
     * This method receives a payment request along with an Idempotency-Key as a header.
     * It ensures idempotency in payment processing, such that if the same request is received more
     * than once (determined by the Idempotency-Key), the server will respond with the result of
     * the previous request without reprocessing the payment.
     * </p>
     *
     * <p>The payment process is as follows:
     * <ul>
     *     <li>If the Idempotency-Key is missing or empty, the method responds with a 400 Bad Request
     *         status and an error message.</li>
     *     <li>If the payment request fails validation, the method responds with a 400 Bad
     *         Request status and an error message.</li>
     *     <li>If a response for the given Idempotency-Key is cached, the method returns the cached
     *         response with a 304 Not Modified status.</li>
     *     <li>If no cached response is found, the method processes the payment, stores the response
     *         with the Idempotency-Key, and returns the payment response with a 200 OK status.</li>
     * </ul>
     * </p>
     *
     * @param idempotencyKeyOptional An {@link Optional} containing the Idempotency-Key, which is used
     *                               to ensure that the payment request is not processed more than once.
     * @param paymentRequest         The {@link PaymentRequest} object containing the details of the
     *                               payment to be processed.
     * @return A {@link ResponseEntity} object containing a {@link PaymentResponse} and the appropriate
     *         HTTP status code. The Idempotency-Key is also included in the response headers.
     */
    @PostMapping
    public ResponseEntity<PaymentResponse> processPayment(@RequestHeader(value = "Idempotency-Key") Optional<String> idempotencyKeyOptional,
                                                          @RequestBody @Valid PaymentRequest paymentRequest) {
        log.debug("Received payment request: {}", paymentRequest);
        if (idempotencyKeyOptional.isEmpty() || idempotencyKeyOptional.get().trim().isEmpty()) {
            log.error("Validation error: Empty Idempotency-Key");
            final PaymentResponse paymentResponse = new PaymentResponse(PaymentStatus.FAILED, "Idempotency-Key is required");
            return ResponseEntity.badRequest().body(paymentResponse);
        }

        if (!paymentRequest.getCard().isCvvValid()) {
            log.error("Validation error: Invalid CVV");
            final PaymentResponse paymentResponse = new PaymentResponse(PaymentStatus.FAILED, "Invalid CVV");
            return ResponseEntity.badRequest().body(paymentResponse);
        }

        final String idempotencyKey = SecurityUtil.cleanIt(idempotencyKeyOptional.get());
        final String merchantId = SecurityUtil.cleanIt(paymentRequest.getMerchantId());
        final String requestKey = idempotencyKey + merchantId;
        // Check if we've seen this idempotency key before
        Optional<PaymentResponse> cachedResponse = idempotencyService.getResponse(requestKey);

        if (cachedResponse.isPresent()) {
            //Sanitize the idempotency key before logging
            log.info("Returning cached response for idempotency key {} and merchant {}", idempotencyKey, merchantId);
            return  ResponseEntity.ok()
                    .header("Idempotency-Key", idempotencyKey)
                    .body(cachedResponse.get());
        }

        final PaymentResponse paymentResponse = paymentService.processPayment(paymentRequest);

        // Store the result with the idempotency key
        idempotencyService.storeResponse(requestKey, paymentResponse);

        return ResponseEntity.ok()
                .header("Idempotency-Key", idempotencyKey)
                .body(paymentResponse);
    }

}
