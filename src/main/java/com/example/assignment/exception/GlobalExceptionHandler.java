package com.example.assignment.exception;

import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import lombok.extern.log4j.Log4j2;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;

@ControllerAdvice
@Log4j2
public class GlobalExceptionHandler {

    /**
     * Handles exceptions thrown when validation fails.
     * <p>
     * This method is annotated with {@code @ExceptionHandler}, meaning it will intercept
     * any {@link MethodArgumentNotValidException} thrown during the processing of a controller method.
     * It logs the validation error and returns a {@link ResponseEntity} with a {@link PaymentResponse}
     * indicating the failure and details of the validation error.
     * </p>
     *
     * @param ex The {@link MethodArgumentNotValidException} exception thrown when validation fails.
     * @return A {@link ResponseEntity} containing a {@link PaymentResponse} with details of the validation error,
     *         and a {@link HttpStatus#BAD_REQUEST} status.
     */
    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<PaymentResponse> handleValidationExceptions(MethodArgumentNotValidException ex) {
        if (!ex.getAllErrors().isEmpty()) {
            log.error("Validation error: {}", ex.getAllErrors().get(0).getDefaultMessage());
        } else {
            log.error("Validation error: {}", ex.getMessage());
        }
        return new ResponseEntity<>(new PaymentResponse(PaymentStatus.FAILED, "Validation error"), HttpStatus.BAD_REQUEST);
    }

    /**
     * Handles exceptions thrown when an HTTP message is not readable.
     * <p>
     * This method is annotated with {@code @ExceptionHandler}, meaning it will intercept
     * any {@link HttpMessageNotReadableException} thrown during the processing of a controller method.
     * It logs the error and returns a {@link ResponseEntity} with a {@link PaymentResponse}
     * indicating a malformed request body.
     * </p>
     *
     * @param ex The {@link HttpMessageNotReadableException} exception thrown when an HTTP message is not readable.
     * @return A {@link ResponseEntity} containing a {@link PaymentResponse} with details of the error,
     *         and a {@link HttpStatus#BAD_REQUEST} status.
     */
    @ExceptionHandler(HttpMessageNotReadableException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ResponseEntity<PaymentResponse> handleHttpMessageNotReadableException(HttpMessageNotReadableException ex) {
        log.error("Validation error: {}", ex.getMessage());
        return new ResponseEntity<>(new PaymentResponse(PaymentStatus.FAILED, "Malformed request body"), HttpStatus.BAD_REQUEST);
    }

}
