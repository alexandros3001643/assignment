package com.example.assignment.model;

import com.example.assignment.model.validation.LuhnCheck;
import com.example.assignment.model.validation.NotExpired;
import com.example.assignment.model.validation.SupportedCardType;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Pattern;
import jakarta.validation.constraints.Size;
import lombok.AccessLevel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Data
@NoArgsConstructor
public class Card {

    @NotNull
    @LuhnCheck
    @Size(min = 12, max = 19, message = "Card number must be between 12 and 19 digits")
    private String cardNumber;

    @NotNull
    @Pattern(regexp = "^(0[1-9]|1[0-2])/(\\d{2})$", message = "Expiry date must be in MM/YY format")
    @NotExpired
    private String expiryDate;

    @NotNull
    @Pattern(regexp = "^[0-9]{3,4}$", message = "CVV must be 3 or 4 digits and numeric")
    private String cvv;

    @NotNull
    @SupportedCardType
    @Setter(value= AccessLevel.PACKAGE)
    private CardType cardType;

    public void setCardNumber(String cardNumber) {
        setCardType(CardType.determineCardType(cardNumber));
        this.cardNumber = cardNumber;
    }

    /**
     * Validates the Card Verification Value (CVV) based on the card type.
     * <p>
     * This method checks if the CVV is valid according to the type of card. The rules are as follows:
     * <ul>
     *     <li>If the card is of type AMEX (American Express), the CVV should be 4 digits long.</li>
     *     <li>For all other card types, the CVV should be 3 digits long.</li>
     * </ul>
     * </p>
     * <p>
     * The method returns false if:
     * <ul>
     *     <li>The CVV is null.</li>
     *     <li>The CVV length does not match the required length for the given card type.</li>
     * </ul>
     * </p>
     *
     * @return true if the CVV is valid according to the card type, false otherwise.
     */
    public boolean isCvvValid() {
        return (cvv != null) && ((cardType == CardType.AMEX && cvv.length() == 4) || (cardType != CardType.AMEX && cvv.length() == 3));
    }
}
