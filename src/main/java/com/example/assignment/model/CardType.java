package com.example.assignment.model;

import lombok.Getter;

@Getter
public enum CardType {
    VISA("^4[0-9]{12}(?:[0-9]{3})?$"),
    MASTERCARD("^(5[1-5][0-9]{14}|2[2-7][0-9]{14})$"),
    AMEX("^3[47][0-9]{13}$"), //American Express
    DISCOVER("^6(?:011|5[0-9]{2}|4[4-9][0-9]|22(?:1[2-9]|[2-8][0-9]|9[01]))[0-9]{12}$"),
    JCB("^(?:2131|1800|35\\d{3})\\d{11}$"),
    MAESTRO("^(5[0-9]{2}|6[0-9]{2})([0-9]{6,13})$"),
    UNKNOWN("");

    private final String regex;

    CardType(String regex) {
        this.regex = regex;
    }

    /**
     * Determines the type of  credit or debit card based on its number.
     * <p>
     * This method uses regular expressions to match the card number against known patterns for various card types.
     * The card type is identified based on the Issuer Identification Number (IIN), also known as the Bank Identification
     * Number (BIN), which are the initial digits of the card number.
     * </p>
     * <p>
     * The method supports the following card types:
     * <ul>
     *     <li>VISA: 13 or 16 digits, starts with 4</li>
     *     <li>MasterCard: 16 digits, starts with 51-55 or 2221-2720</li>
     *     <li>American Express (AMEX): 15 digits, starts with 34 or 37</li>
     *     <li>Discover: 16 digits, various starting patterns</li>
     *     <li>JCB: 16 digits, starts with 2131, 1800, or 35</li>
     *     <li>Maestro: 12 to 19 digits, starts with 50-58 or 6</li>
     * </ul>
     * </p>
     * <p>
     * If the card number is {@code null} or does not match any of the known patterns, the method returns
     * {@link CardType#UNKNOWN}.
     * </p>
     *
     * @param cardNumber The card number to determine the type of.
     * @return The determined {@link CardType}, or {@link CardType#UNKNOWN} if the card type could not be determined.
     */
    public static CardType determineCardType(String cardNumber) {
        if (cardNumber == null) {
            return CardType.UNKNOWN;
        }
        for (CardType cardType : CardType.values()) {
            if (cardNumber.matches(cardType.getRegex())) {
                return cardType;
            }
        }
        return UNKNOWN;
    }

}
