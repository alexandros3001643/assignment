package com.example.assignment.model;

import com.fasterxml.jackson.annotation.JsonValue;

public enum PaymentStatus {

    APPROVED("Approved"),
    DENIED("Denied"),
    FAILED("Failed");

    private final String label;

    PaymentStatus(String label) {
       this.label = label;
    }
    @JsonValue
    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }
}
