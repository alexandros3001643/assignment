package com.example.assignment.model.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;

public class ExpiryDateValidator implements ConstraintValidator<NotExpired, String> {

    /**
     * Validates that a given string represents a valid expiry date for a credit or debit card.
     * <p>
     * This method checks if the input string represents a date that is either the current month/year
     * or a future month/year, according to the "MM/yy" pattern. The validation considers the month and year
     * components only, ignoring days or time if provided.
     * </p>
     * <p>
     * For example, if the current date is October 2023:
     * <ul>
     *     <li>"10/23" returns true (valid for the current month)</li>
     *     <li>"11/23" returns true (valid for a future month in the same year)</li>
     *     <li>"09/23" returns false (invalid as it's a past month)</li>
     *     <li>"13/23" returns false (invalid as it's not a valid month)</li>
     * </ul>
     * </p>
     * <p>
     * If the input string is null or empty, or if it cannot be parsed according to the "MM/yy" pattern,
     * the method returns false.
     * </p>
     *
     * @param value   The string value representing the expiry date to be validated.
     * @param context The context in which the constraint is evaluated, not used in this implementation.
     * @return true if the input string represents a valid future or current expiry date according to the "MM/yy" pattern,
     *         false otherwise.
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.isEmpty()) {
            return false;  // Consider null or empty value as invalid
        }

        try {
            final DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MM/yy");
            final YearMonth expiryDate = YearMonth.parse(value, formatter);
            final YearMonth currentDate = YearMonth.now();
            return  expiryDate.equals(currentDate) || expiryDate.isAfter(currentDate);  // Return true if the expiry date is after the current date
        } catch (DateTimeParseException e) {
            return false;  // Return false if the date cannot be parsed
        }
    }
}

