package com.example.assignment.model.validation;

import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class LuhnCheckValidator implements ConstraintValidator<LuhnCheck, String> {

    /**
     * Validates a credit or debit card number using the Luhn algorithm.
     * <p>
     * The Luhn algorithm is a simple checksum formula used to validate a variety of identification numbers,
     * such as credit card numbers, IMEI numbers, and National Provider Identifier numbers in the US.
     * </p>
     * <p>
     * The method takes a string as input, representing the card number, and checks:
     * <ul>
     *     <li>if it is null or not within the range of 12 to 19 digits (both inclusive), it returns false.</li>
     *     <li>if it contains any non-numeric characters, it returns false.</li>
     *     <li>if it passes the Luhn algorithm check, it returns true; otherwise, it returns false.</li>
     * </ul>
     * </p>
     *
     * @param value   The credit or debit card number as a string.
     * @param context The context in which the constraint is evaluated, not used in this implementation.
     * @return true if the input string represents a valid credit or debit card number according to the Luhn algorithm,
     *         false otherwise.
     */
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        if (value == null || value.length() < 12 || value.length() > 19) {
            return false;
        }
        int sum = 0;
        boolean alternate = false;
        for (int i = value.length() - 1; i >= 0; i--) {
            int n;
            try {
                n = Integer.parseInt(value.substring(i, i + 1));
            } catch (NumberFormatException e) {
                return false;  // Return false if a non-numeric character is found
            }
            if (alternate) {
                n *= 2;
                if (n > 9) {
                    n = (n % 10) + 1;
                }
            }
            sum += n;
            alternate = !alternate;
        }
        return (sum % 10 == 0);
    }
}
