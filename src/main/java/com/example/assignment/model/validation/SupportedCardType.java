package com.example.assignment.model.validation;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import jakarta.validation.Constraint;
import jakarta.validation.Payload;

@Target({ ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = SupportedCardTypeValidator.class)
public @interface SupportedCardType {
    String message() default "Card type is not supported";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}

