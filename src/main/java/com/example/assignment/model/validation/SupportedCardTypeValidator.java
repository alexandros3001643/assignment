package com.example.assignment.model.validation;

import com.example.assignment.model.CardType;
import jakarta.validation.ConstraintValidator;
import jakarta.validation.ConstraintValidatorContext;

public class SupportedCardTypeValidator implements ConstraintValidator<SupportedCardType, CardType> {

    /**
     * Validates that a given {@link CardType} is supported.
     * <p>
     * This method checks if the input {@code CardType} is among the supported card types.
     * Supported card types are defined within the method and typically include major credit and
     * debit card providers.
     * </p>
     * <p>
     * The supported card types in this implementation are VISA, MASTERCARD, AMEX, DISCOVER, JCB,
     * and MAESTRO. If the card type is UNKNOWN or any other type not listed as supported,
     * the method returns false.
     * </p>
     *
     * @param cardType The {@link CardType} representing the type of card to be validated.
     * @param context  The context in which the constraint is evaluated, not used in this implementation.
     * @return true if the card type is supported, false otherwise.
     */
    @Override
    public boolean isValid(CardType cardType, ConstraintValidatorContext context) {
        if (cardType == null) {
            return false; // Consider null as not valid. You can change this based on your requirements.
        }

        // List supported card types here
        return switch (cardType) {
            case VISA, MASTERCARD, AMEX, DISCOVER, JCB, MAESTRO -> true;
            case UNKNOWN -> false; // Assume UNKNOWN is not supported
        };
    }
}

