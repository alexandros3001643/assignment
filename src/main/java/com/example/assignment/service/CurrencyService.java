package com.example.assignment.service;

import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class CurrencyService {

    public BigDecimal convert(String currency, String baseCurrency, Double amount) {
        // In a real application, this method would perform the actual currency conversion
        return BigDecimal.valueOf(amount);
    }
}
