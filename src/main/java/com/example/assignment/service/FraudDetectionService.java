package com.example.assignment.service;

import com.example.assignment.model.PaymentRequest;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class FraudDetectionService {

    // For example, 10,000 in BASE_CURRENCY
    private static final BigDecimal TRANSACTION_THRESHOLD = new BigDecimal("10000");
    private static final String BASE_CURRENCY = "USD";

    private final CurrencyService currencyService;

    public FraudDetectionService(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    /**
     * Checks if a payment request exceeds the defined threshold.
     *
     * @param paymentRequest The payment request to check.
     * @return true if the request is deemed fraudulent, false otherwise.
     */
    public boolean isFraudulent(PaymentRequest paymentRequest) {
        // Convert the amount to the base currency
        BigDecimal amountInBaseCurrency = currencyService.convert(paymentRequest.getCurrency(), BASE_CURRENCY,
                paymentRequest.getAmount());
        // Check if the amount in base currency exceeds the threshold
        return TRANSACTION_THRESHOLD.compareTo(amountInBaseCurrency) < 0;
    }
}

