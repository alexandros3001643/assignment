package com.example.assignment.service;

import com.example.assignment.model.PaymentResponse;

import java.util.Optional;

public interface IdempotencyService {
    Optional<PaymentResponse> getResponse(String idempotencyKey);
    void storeResponse(String idempotencyKey, PaymentResponse response);
}

