package com.example.assignment.service;

import com.example.assignment.model.PaymentResponse;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class InMemoryIdempotencyService implements IdempotencyService {

    private final Map<String, PaymentResponse> idempotencyCache = new ConcurrentHashMap<>();

    /**
     * Retrieves a previously stored payment response associated with the given idempotency key.
     * <p>
     * This method looks up the idempotency cache to fetch the payment response corresponding to the provided
     * idempotency key. Idempotency keys are unique identifiers for payment requests to ensure that the same
     * payment is not processed multiple times.
     * </p>
     * <p>
     * If a payment response is found in the cache associated with the given idempotency key, it is returned;
     * otherwise, an empty {@link Optional} is returned.
     * </p>
     *
     * @param idempotencyKey The unique identifier for a payment request.
     * @return An {@link Optional} containing the {@link PaymentResponse} if found, or an empty Optional otherwise.
     */
    @Override
    public Optional<PaymentResponse> getResponse(String idempotencyKey) {
        return Optional.ofNullable(idempotencyCache.get(idempotencyKey));
    }

    /**
     * Stores a payment response in the idempotency cache associated with the given idempotency key.
     * <p>
     * This method is used to store the result of a payment transaction, referenced by a unique idempotency key,
     * to ensure that repeated payment requests with the same idempotency key do not result in multiple
     * transactions.
     * </p>
     * <p>
     * Storing the payment response allows future requests with the same idempotency key to immediately return
     * the stored result, providing idempotency in the payment processing.
     * </p>
     *
     * @param idempotencyKey The unique identifier for a payment request.
     * @param response The payment response to store.
     */
    @Override
    public void storeResponse(String idempotencyKey, PaymentResponse response) {
        idempotencyCache.put(idempotencyKey, response);
    }
}

