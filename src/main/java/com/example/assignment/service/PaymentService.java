package com.example.assignment.service;

import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import com.example.assignment.service.psp.PaymentServiceProvider;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PaymentService {

    private final PspRouter pspRouter;

    private final FraudDetectionService fraudDetectionService;

    @Autowired
    public PaymentService(PspRouter pspRouter, FraudDetectionService fraudDetectionService) {
        this.pspRouter = pspRouter;
        this.fraudDetectionService = fraudDetectionService;
    }

    /**
     * Processes a payment request by first conducting a fraud check, then routing it to the appropriate
     * payment service provider (PSP), and finally executing the payment transaction.
     * <p>
     * Initially, this method assesses the payment request for potential fraud using a dedicated fraud detection
     * service. If the transaction is flagged as potentially fraudulent, the method immediately returns a
     * {@link PaymentResponse} indicating a denial of the transaction due to fraud concerns.
     * </p>
     * <p>
     * If the transaction passes the fraud check, the method proceeds to determine the appropriate PSP for
     * handling the payment request. This routing is performed by the {@link PspRouter} component, which
     * encapsulates the logic for selecting the most suitable PSP based on the characteristics of the payment request.
     * </p>
     * <p>
     * Upon determining the suitable PSP, this method delegates the responsibility of processing the payment
     * to the selected PSP’s {@code processPayment} method. The outcome of the payment transaction is then
     * returned as a {@link PaymentResponse} object.
     * </p>
     *
     * @param paymentRequest The payment request containing all the necessary details for processing the payment.
     * @return A {@link PaymentResponse} object representing the outcome of the payment transaction.
     */
    public PaymentResponse processPayment(PaymentRequest paymentRequest) {
        // Check for potential fraud
        if (fraudDetectionService.isFraudulent(paymentRequest)) {
            log.warn("Transaction flagged as potential fraud {}", paymentRequest);
            return new PaymentResponse(PaymentStatus.DENIED, "Transaction denied");
        }
        // Get the PSP provider
        PaymentServiceProvider paymentServiceProvider = pspRouter.route(paymentRequest);

        // Process the payment (this is a simplified example)
        // Return the response
        return paymentServiceProvider.processPayment(paymentRequest);
    }
}
