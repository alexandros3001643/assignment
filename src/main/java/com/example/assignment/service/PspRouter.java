package com.example.assignment.service;

import com.example.assignment.model.CardType;
import com.example.assignment.model.PaymentRequest;
import com.example.assignment.service.balancer.MonetaryVolumeBalancer;
import com.example.assignment.service.balancer.RoundRobinBalancer;
import com.example.assignment.service.psp.PaymentServiceProvider;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
@Log4j2
public class PspRouter {

    private final PaymentServiceProvider psp1Service;
    private final RoundRobinBalancer roundRobinBalancer;

    private final MonetaryVolumeBalancer monetaryVolumeBalancer;
    @Autowired
    public PspRouter(@Qualifier("psp1Service") PaymentServiceProvider psp1Service,
                     RoundRobinBalancer roundRobinBalancer,
                     MonetaryVolumeBalancer monetaryVolumeBalancer) {
        this.psp1Service = psp1Service;
        this.roundRobinBalancer = roundRobinBalancer;
        this.monetaryVolumeBalancer = monetaryVolumeBalancer;
    }

    /**
     * Routes a payment request to the appropriate payment service provider (PSP) based on specific routing logic.
     * <p>
     * This method implements the routing logic to determine which PSP should handle a given payment request. The decision
     * is based on the card type and currency specified in the payment request. The method is synchronized to ensure
     * thread-safe operation, which is crucial in a payment processing environment to prevent race conditions.
     * </p>
     * <p>
     * For example, if the card type is VISA and the currency is EUR, it uses the {@link com.example.assignment.service.psp.Psp1Service}. If the card type
     * is MASTERCARD and the currency is USD, it uses the {@link MonetaryVolumeBalancer} to determine the provider. For all other
     * cases, it uses the {@link RoundRobinBalancer} to distribute the load evenly among available PSPs.
     * </p>
     * <p>
     * After selecting a PSP, it prepares the provider for payment processing by updating the monetary volume
     * information to avoid overloading a provider, and then returns the selected PSP.
     * </p>
     *
     * @param request The payment request containing details such as card type and currency.
     * @return The selected {@link PaymentServiceProvider} to handle the payment request.
     */
    public synchronized PaymentServiceProvider route(PaymentRequest request) {
        final String currency = request.getCurrency();
        final CardType cardType = request.getCard().getCardType();
        // Implement routing logic based on paymentRequest details
        PaymentServiceProvider provider;
        if (CardType.VISA.equals(cardType) && "EUR".equals(request.getCurrency())) {
            log.info("Card type is VISA and currency is EUR");
            log.info("Using psp1Service");
            provider = psp1Service;
        } else if (CardType.MASTERCARD.equals(cardType) && "USD".equals(request.getCurrency())) {
            log.info("Card type is MASTERCARD and currency is USD");
            log.info("Using monetary volume balancer");
            provider = monetaryVolumeBalancer.balance(currency);
        } else {
            log.info("Using round robin balancer");
            provider = roundRobinBalancer.balance();
        }
        log.debug("Selected provider {} with monetary volume {} for currency {}",
                provider.getIdentifier(), provider.getMonetaryVolume(currency), currency);

        provider.preparePaymentProcessing(request);

        return provider;
    }
}

