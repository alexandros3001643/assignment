package com.example.assignment.service.balancer;

import com.example.assignment.service.psp.PaymentServiceProvider;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Component;

import java.util.Comparator;
import java.util.List;

@Component
@Log4j2
public class MonetaryVolumeBalancer {

    private final List<PaymentServiceProvider> providers;

    public MonetaryVolumeBalancer(List<PaymentServiceProvider> providers) {
        this.providers = providers.stream()
                .filter(p -> p.getIdentifier().equals("Psp1Service") || p.getIdentifier().equals("Psp2Service"))
                .toList();
        if (this.providers.size() < 2) {
            log.error("Psp1Service and Psp2Service are required for this balancer");
            throw new RuntimeException("Psp1Service and Psp2Service are required for the monetary volume balancer");
        }
    }

    /**
     * Finds and returns the payment service provider with the lowest monetary volume for a specified currency.
     * <p>
     * This method evaluates all the available payment service providers, calculating their monetary volume
     * based on the specified currency, and returns the provider with the minimum monetary volume.
     * The monetary volume indicates the total value of transactions managed by a provider in a particular currency.
     * </p>
     * <p>
     * This method is synchronized, ensuring thread-safety when accessing shared resources. This is crucial in
     * a multi-threaded environment to prevent data inconsistency and to ensure that every thread gets the
     * correct provider.
     * </p>
     * <p>
     * If no providers are available or if an error occurs during the retrieval, a {@link RuntimeException} is thrown,
     * and an error message is logged.
     * </p>
     *
     * @param currency The currency used to calculate the monetary volume of each provider.
     * @return The payment service provider with the lowest monetary volume for the specified currency.
     * @throws RuntimeException If no payment service providers are available.
     */
    public synchronized PaymentServiceProvider balance(final String currency) {
        return providers.stream()
                .min(Comparator.comparingDouble(serviceProvider -> serviceProvider.getMonetaryVolume(currency).doubleValue()))
                .orElseThrow(() -> {
                    log.error("No available payment service providers");
                    return new RuntimeException("No available payment service providers");
                });
    }
}

