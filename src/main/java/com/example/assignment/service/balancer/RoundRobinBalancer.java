package com.example.assignment.service.balancer;

import com.example.assignment.service.psp.PaymentServiceProvider;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@Component
public class RoundRobinBalancer {

    private final AtomicInteger index = new AtomicInteger(0);
    private final List<PaymentServiceProvider> providers;

    public RoundRobinBalancer(List<PaymentServiceProvider> providers) {
        this.providers = providers;
    }

    /**
     * Provides a thread-safe way to obtain a payment service provider using round-robin selection.
     * <p>
     * This method implements a round-robin algorithm to distribute the load evenly among the available
     * payment service providers. Each call to this method returns the next provider in the list,
     * cycling back to the first provider when the end of the list is reached.
     * </p>
     * <p>
     * The method ensures thread safety by using a synchronized block and an atomic integer to keep
     * track of the current index in the provider list. This makes the method suitable for use in
     * multi-threaded environments where multiple threads might be trying to obtain a provider at the
     * same time.
     * </p>
     * <p>
     * If no providers are available, this method will throw an {@link IndexOutOfBoundsException}.
     * </p>
     *
     * @return The next payment service provider in the round-robin sequence.
     * @throws IndexOutOfBoundsException If the list of providers is empty.
     */
    public synchronized PaymentServiceProvider balance() {
        final int currentIndex = index.getAndUpdate(i -> (i + 1) % providers.size());
        return providers.get(currentIndex);
    }
}

