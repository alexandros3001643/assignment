package com.example.assignment.service.psp;

import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import lombok.extern.log4j.Log4j2;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.atomic.DoubleAdder;
@Log4j2
public abstract class PaymentServiceProvider {
    private final ConcurrentMap<String, DoubleAdder> currencyAmountMap;

    protected PaymentServiceProvider() {
        this.currencyAmountMap = new ConcurrentHashMap<>();
    }

    /**
     * Prepares the payment service provider for processing a payment.
     * <p>
     * This method proactively updates the monetary volume of the provider based on the currency and amount specified
     * in the provided {@link PaymentRequest}. The monetary volume is a measure of the total transaction value
     * handled by this provider in a specific currency.
     * </p>
     * <p>
     * Updating the monetary volume prior to processing helps in load balancing and avoids overloading any single provider.
     * In case the subsequent transaction fails, it is expected that the monetary volume update will be reverted
     * to maintain accuracy.
     * </p>
     *
     * @param paymentRequest The payment request containing the details of the transaction to be processed.
     *                       It includes the currency and the amount that will be used to update the monetary volume.
     */
    public void preparePaymentProcessing(PaymentRequest paymentRequest) {
        log.info("Preparing payment processing in processor {} with monetary volume {} for currency {}",
                getIdentifier(), getMonetaryVolume(paymentRequest.getCurrency()), paymentRequest.getCurrency());
        // Update the monetary volume for the given currency
        getMonetaryVolume(paymentRequest.getCurrency()).add(paymentRequest.getAmount());
    }
    /**
     * Processes a payment request and returns the corresponding payment response.
     * <p>
     * This method processes the payment based on the provided {@link PaymentRequest}.
     * The payment processing logic in this implementation is simplified, basing the approval or denial of a transaction
     * on the last digit of the provided card number. If the last digit is even, the transaction is approved;
     * otherwise, it is denied.
     * </p>
     * <p>
     * In case of a denied transaction, the monetary volume that was previously updated in the {@code preparePaymentProcessing}
     * method is reverted to maintain accuracy.
     * </p>
     *
     * @param paymentRequest The payment request containing the details of the transaction to be processed.
     * @return A {@link PaymentResponse} object representing the outcome of the payment processing.
     */
    public PaymentResponse processPayment(PaymentRequest paymentRequest) {
        log.info("Processing payment request in processor {} with monetary volume {} for currency {}",
                getIdentifier(), getMonetaryVolume(paymentRequest.getCurrency()), paymentRequest.getCurrency());
        // Implement the logic based on the card number's last digit
        final String cardNumber = paymentRequest.getCard().getCardNumber();
        final int lastDigit = Character.getNumericValue(cardNumber.charAt(cardNumber.length() - 1));
        if (lastDigit % 2 == 0) {
            log.info("Transaction approved");
            return new PaymentResponse(PaymentStatus.APPROVED, "Transaction successful");
        } else {
            log.info("Transaction denied");
            // Subtract the failed transaction from the monetary volume that was added during preparePaymentProcessing
            final String currency = paymentRequest.getCurrency();
            final double amount = paymentRequest.getAmount();
            getMonetaryVolume(currency).add(amount * -1);
            return new PaymentResponse(PaymentStatus.DENIED, "Transaction denied");
        }
    }

    public DoubleAdder getMonetaryVolume(String currency) {
        return currencyAmountMap.computeIfAbsent(currency, k -> new DoubleAdder());
    }

    public String getIdentifier() {
        return this.getClass().getSimpleName();
    }
}
