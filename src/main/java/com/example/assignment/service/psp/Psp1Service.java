package com.example.assignment.service.psp;

import org.springframework.stereotype.Service;

@Service("psp1Service")
public class Psp1Service extends PaymentServiceProvider {
    public Psp1Service() {
        super();
    }
}