package com.example.assignment.service.psp;

import org.springframework.stereotype.Service;

@Service("psp2Service")
public class Psp2Service extends PaymentServiceProvider {
    public Psp2Service() {
        super();
    }
}