package com.example.assignment.service.psp;

import org.springframework.stereotype.Service;

@Service("psp3Service")
public class Psp3Service extends PaymentServiceProvider {
    public Psp3Service() {
        super();
    }
}