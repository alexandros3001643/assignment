package com.example.assignment;

import com.example.assignment.model.Card;
import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import lombok.extern.log4j.Log4j2;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.*;

import static org.assertj.core.api.Fail.fail;
import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@Log4j2
public class ConcurrencyIntegrationTest {

    @Autowired
    private TestRestTemplate restTemplate;

    @Test
    public void testConcurrentPayments() throws InterruptedException {
        int numberOfThreads = 100;
        CountDownLatch latch = new CountDownLatch(numberOfThreads);
        ExecutorService executorService = Executors.newFixedThreadPool(numberOfThreads);

        List<Future<ResponseEntity<PaymentResponse>>> futures = new ArrayList<>();
        for (int i = 0; i < numberOfThreads; i++) {
            int finalI = i;
            Future<ResponseEntity<PaymentResponse>> future = executorService.submit(() -> {
                try {
                    latch.countDown();
                    latch.await(); // Ensures all threads are ready to go at the same time
                    PaymentRequest paymentRequest = createValidPaymentRequest();
                    return restTemplate.postForEntity("/api/payment",
                            new HttpEntity<>(paymentRequest, createHeaders("Idempotency-Key", "unique-key-" + finalI)), PaymentResponse.class);
                } catch (InterruptedException e) {
                    throw new RuntimeException(e);
                }
            });
            futures.add(future);
        }

        for (Future<ResponseEntity<PaymentResponse>> future : futures) {
            try {
                ResponseEntity<PaymentResponse> response = future.get();
                assertEquals(HttpStatus.OK, response.getStatusCode(), Objects.requireNonNull(response.getBody()).getMessage());
                assertEquals(PaymentStatus.APPROVED, response.getBody().getStatus());
            } catch (ExecutionException e) {
                fail("Execution failed: " + e.getMessage());
            }
        }

        executorService.shutdown();
        executorService.awaitTermination(10, TimeUnit.SECONDS);
    }

    private HttpHeaders createHeaders(String key, String value) {
        HttpHeaders headers = new HttpHeaders();
        headers.set(key, value);
        return headers;
    }

    private PaymentRequest createValidPaymentRequest() {
        PaymentRequest request = new PaymentRequest();
        Card card = new Card();
        card.setCardNumber("5111010030175156");  // Valid Visa card number
        card.setExpiryDate("12/99");  // Valid future date
        card.setCvv("123");  // Valid CVV
        request.setCard(card);
        request.setAmount(100.0);  // Valid amount
        request.setCurrency("USD");  // Valid currency
        request.setMerchantId("merchant123");  // Valid merchant ID
        return request;
    }
}

