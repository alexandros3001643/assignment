package com.example.assignment.controller;

import com.example.assignment.model.Card;
import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import com.example.assignment.service.InMemoryIdempotencyService;
import com.example.assignment.service.PaymentService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.boot.test.mock.mockito.SpyBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import java.util.Optional;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


@ExtendWith(SpringExtension.class)
@WebMvcTest(PaymentController.class)
public class PaymentControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private PaymentService paymentService;

    @SpyBean
    private InMemoryIdempotencyService idempotencyService;

    private PaymentRequest createValidPaymentRequest() {
        PaymentRequest request = new PaymentRequest();
        Card card = new Card();
        card.setCardNumber("4111111111111111");  // Valid Visa card number
        card.setExpiryDate("12/99");  // Valid future date
        card.setCvv("123");  // Valid CVV
        request.setCard(card);
        request.setAmount(100.0);  // Valid amount
        request.setCurrency("USD");  // Valid currency
        request.setMerchantId("merchant123");  // Valid merchant ID
        return request;
    }

    @Test
    public void whenPostPayment_thenStatusOk() throws Exception {
        PaymentRequest request = createValidPaymentRequest();
        PaymentResponse response = new PaymentResponse(PaymentStatus.APPROVED, "Transaction was successful");

        when(paymentService.processPayment(any(PaymentRequest.class))).thenReturn(response);

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "unique-key")
                        .content(asJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("Approved")));

        // Verify that the idempotency key is stored with the response
        verify(idempotencyService, times(1)).storeResponse(eq("unique-key" + request.getMerchantId()), any(PaymentResponse.class));
    }

    @Test
    public void whenPostPayment_thenValidatedCardRequest() throws Exception {
        PaymentRequest request = createValidPaymentRequest();
        request.getCard().setExpiryDate("12/20");  // Past date

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "unique-key")
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Failed")))
                .andExpect(jsonPath("$.message", is("Validation error")));
    }

    @Test
    public void processPayment_WhenIdempotencyKeyIsMissing_ReturnBadRequest() throws Exception {
        PaymentRequest request = createValidPaymentRequest();

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Failed")))
                .andExpect(jsonPath("$.message", is("Idempotency-Key is required")));
    }

    @Test
    public void processPayment_WhenIdempotencyKeyIsEmpty_ReturnBadRequest() throws Exception {
        PaymentRequest request = createValidPaymentRequest();

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "")
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Failed")))
                .andExpect(jsonPath("$.message", is("Idempotency-Key is required")));
    }

    @Test
    public void processPayment_WhenPaymentRequestIsMissing_ReturnBadRequest() throws Exception {
        PaymentRequest request = new PaymentRequest();

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Failed")))
                .andExpect(jsonPath("$.message", is("Validation error")));
    }

    @Test
    public void processPayment_WhenCVVIsWrong_ReturnBadRequest() throws Exception {
        PaymentRequest request = createValidPaymentRequest();
        request.getCard().setCvv("1234");  // Invalid CVV

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "unique-key")
                        .content(asJsonString(request)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is("Failed")))
                .andExpect(jsonPath("$.message", is("Invalid CVV")));

        // Verify that the idempotency key is not stored
        verify(idempotencyService, never()).storeResponse(anyString(), any(PaymentResponse.class));
    }

    @Test
    public void processPayment_WhenIdempotencyKeyIsInCache_ReturnOk() throws Exception {
        PaymentRequest request = createValidPaymentRequest();
        PaymentResponse cachedResponse = new PaymentResponse(PaymentStatus.APPROVED, "Transaction was successful");

        when(idempotencyService.getResponse(anyString())).thenReturn(Optional.of(cachedResponse));

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "unique-key")
                        .content(asJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("Approved")));
    }

    @Test
    public void processPayment_SameIdempotencyKeyDifferentMerchant() throws Exception {
        PaymentRequest request = createValidPaymentRequest();
        PaymentResponse response = new PaymentResponse(PaymentStatus.APPROVED, "Transaction was successful");
        when(paymentService.processPayment(any(PaymentRequest.class))).thenReturn(response);

        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "unique-key")
                        .content(asJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("Approved")))
                .andExpect(header().string("Idempotency-Key", "unique-key"));

        request.setMerchantId("merchant456");

        response = new PaymentResponse(PaymentStatus.DENIED, "");
        when(paymentService.processPayment(any(PaymentRequest.class))).thenReturn(response);
        mockMvc.perform(post("/api/payment")
                        .contentType(MediaType.APPLICATION_JSON)
                        .header("Idempotency-Key", "unique-key")
                        .content(asJsonString(request)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.status", is("Denied")))
                .andExpect(header().string("Idempotency-Key", "unique-key"));
    }

    // Convert object to JSON string
    private static String asJsonString(final Object obj) {
        try {
            return new ObjectMapper().writeValueAsString(obj);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
