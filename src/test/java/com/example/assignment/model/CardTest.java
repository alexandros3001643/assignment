package com.example.assignment.model;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;

import java.time.YearMonth;
import java.time.format.DateTimeFormatter;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class CardTest extends ModelTest<Card> {

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    @Test
    void shouldValidateCardNumber() {
        Card card = new Card();

        //Pass Luhn check
        card.setCardNumber("378282246310005");
        assertNoViolations("cardNumber", validator.validate(card));

        //Does not pass Luhn check
        card.setCardNumber("1234567890123456");
        assertHasViolations("cardNumber", validator.validate(card), "Invalid card number");

        // Less than 12 digits
        card.setCardNumber("12345678901");
        assertHasViolations("cardNumber", validator.validate(card), "Card number must be between 12 and 19 digits");

        //More than 19 digits
        card.setCardNumber("12345678901678901246");
        assertHasViolations("cardNumber", validator.validate(card), "Card number must be between 12 and 19 digits");

        card.setCardNumber(null);
        assertHasViolations("cardNumber", validator.validate(card), "must not be null");

        card.setCardNumber("");
        assertHasViolations("cardNumber", validator.validate(card), "Invalid card number");
    }

    @Test
    void shouldValidateExpiryDate() {
        Card card = new Card();

        String now = YearMonth.now().format(DateTimeFormatter.ofPattern("MM/yy"));
        card.setExpiryDate(now);

        assertNoViolations("expiryDate", validator.validate(card));

        String future = YearMonth.now().plusMonths(1).format(DateTimeFormatter.ofPattern("MM/yy"));
        card.setExpiryDate(future);

        assertNoViolations("expiryDate", validator.validate(card));

        String past = YearMonth.now().minusMonths(1).format(DateTimeFormatter.ofPattern("MM/yy"));
        card.setExpiryDate(past);

        assertHasViolations("expiryDate", validator.validate(card), "The card has expired");

        card.setExpiryDate(null);
        assertHasViolations("expiryDate", validator.validate(card), "must not be null");

        card.setExpiryDate("");
        assertHasViolations("expiryDate", validator.validate(card), "Expiry date must be in MM/YY format");
    }

    @Test
    void shouldValidateCVV() {
        Card card = new Card();

        card.setCvv("123");
        assertNoViolations("cvv", validator.validate(card));

        card.setCvv("12345");
        assertHasViolations("cvv", validator.validate(card), "CVV must be 3 or 4 digits and numeric");

        card.setCvv(null);
        assertHasViolations("cvv", validator.validate(card), "must not be null");

        card.setCvv("");
        assertHasViolations("cvv", validator.validate(card), "CVV must be 3 or 4 digits and numeric");
    }
    
    @Test
    void shouldValidateCardType() {
        Card card = new Card();

        card.setCardType(CardType.VISA);
        assertNoViolations("cardType", validator.validate(card));

        card.setCardType(CardType.UNKNOWN);
        assertHasViolations("cardType", validator.validate(card), "Card type is not supported");

        card.setCardType(null);
        assertHasViolations("cardType", validator.validate(card), "must not be null"); // Assuming UNKNOWN or null are treated the same
    }
    
    @Test
    void shouldSetCvvAndValidateBasedOnCardType() {
        Card card = new Card();

        //CVV null
        assertFalse(card.isCvvValid());

        // Not AMEX - 3 digits - Valid
        card.setCvv("123");
        assertTrue(card.isCvvValid());

        // Not AMEX - 3 digits - Invalid
        card.setCvv("1234");
        assertFalse(card.isCvvValid());

        // AMEX - 4 digits - Valid
        card.setCardType(CardType.AMEX);
        card.setCvv("1234");
        assertTrue(card.isCvvValid());

        // AMEX - 3 digits - invalid
        card.setCvv("123");
        assertFalse(card.isCvvValid());
    }
}