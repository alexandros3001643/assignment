package com.example.assignment.model;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class CardTypeTest {

    @Test
    void determineCardTypeShouldReturnUnknownForNullInput() {
        assertEquals(CardType.UNKNOWN, CardType.determineCardType(null));
    }

    @Test
    void determineCardTypeShouldReturnVisaForVisaCard() {
        assertEquals(CardType.VISA, CardType.determineCardType("4111111111111111"));
    }

    @Test
    void determineCardTypeShouldReturnMastercardForMastercardCard() {
        assertEquals(CardType.MASTERCARD, CardType.determineCardType("5111111111111118"));
        assertEquals(CardType.MASTERCARD, CardType.determineCardType("5211111111111117"));
        assertEquals(CardType.MASTERCARD, CardType.determineCardType("5311111111111116"));
        assertEquals(CardType.MASTERCARD, CardType.determineCardType("5411111111111115"));
        assertEquals(CardType.MASTERCARD, CardType.determineCardType("5511111111111114"));
    }

    @Test
    void determineCardTypeShouldReturnAmexForAmexCard() {
        assertEquals(CardType.AMEX, CardType.determineCardType("341111111111111"));
        assertEquals(CardType.AMEX, CardType.determineCardType("371111111111114"));
    }

    @Test
    void determineCardTypeShouldReturnDiscoverForDiscoverCard() {
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6011111111111117"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6441111111111116"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6451111111111115"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6461111111111114"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6471111111111113"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6481111111111112"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6491111111111111"));
        assertEquals(CardType.DISCOVER, CardType.determineCardType("6511111111111110"));
    }

    @Test
    void determineCardTypeShouldReturnJcbForJcbCard() {
        assertEquals(CardType.JCB, CardType.determineCardType("3528111111111119"));
        assertEquals(CardType.JCB, CardType.determineCardType("3589111111111110"));
    }

    @Test
    void determineCardTypeShouldReturnMaestroForMaestroCard() {
        assertEquals(CardType.MAESTRO, CardType.determineCardType("5018111111111112"));
        assertEquals(CardType.MAESTRO, CardType.determineCardType("5611111111111116"));
        assertEquals(CardType.MAESTRO, CardType.determineCardType("5711111111111115"));
        assertEquals(CardType.MAESTRO, CardType.determineCardType("5811111111111114"));
        assertEquals(CardType.MAESTRO, CardType.determineCardType("6711111111111113"));
    }

    @Test
    void determineCardTypeShouldReturnUnknownForOtherCardNumbers() {
        assertEquals(CardType.UNKNOWN, CardType.determineCardType("1234567890123456"));
        assertEquals(CardType.UNKNOWN, CardType.determineCardType("9876543210123456"));
    }

}