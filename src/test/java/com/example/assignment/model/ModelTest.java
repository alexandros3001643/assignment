package com.example.assignment.model;

import jakarta.validation.ConstraintViolation;

import java.util.Optional;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.assertTrue;

public abstract class ModelTest <T> {
    protected void assertNoViolations(String property, Set<ConstraintViolation<T>> violations) {
        assertTrue(violations.stream().noneMatch(v -> v.getPropertyPath().toString().equals(property)));
    }

    protected void assertHasViolations(String property, Set<ConstraintViolation<T>> violations, String violationMessage) {
        Optional<ConstraintViolation<T>> violation = violations.stream()
                .filter(v -> v.getPropertyPath().toString().equals(property))
                .filter(v -> v.getMessage().equals(violationMessage))
                .findFirst();
        assertTrue(violation.isPresent());
    }
}
