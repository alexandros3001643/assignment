package com.example.assignment.model;

import jakarta.validation.Validation;
import jakarta.validation.Validator;
import jakarta.validation.ValidatorFactory;
import org.junit.jupiter.api.Test;

class PaymentRequestTest extends ModelTest<PaymentRequest> {

    private final ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    private final Validator validator = factory.getValidator();

    @Test
    void shouldValidateAmount() {
        PaymentRequest request = new PaymentRequest();

        request.setAmount(100.50);
        assertNoViolations("amount", validator.validate(request));

        request.setAmount(-10.50);
        assertHasViolations("amount", validator.validate(request), "Amount must be positive");
    }

    @Test
    void shouldValidateCurrency() {
        PaymentRequest request = new PaymentRequest();

        request.setCurrency("USD");
        assertNoViolations("currency", validator.validate(request));

        request.setCurrency(null);
        assertHasViolations("currency", validator.validate(request), "must not be null");

        request.setCurrency("");
        assertHasViolations("currency", validator.validate(request), "Currency must be a 3-letter uppercase code");
    }

    @Test
    void shouldValidateMerchantId() {
        PaymentRequest request = new PaymentRequest();

        request.setMerchantId("merchant123");
        assertNoViolations("merchantId", validator.validate(request));

        request.setMerchantId(null);
        assertHasViolations("merchantId", validator.validate(request), "must not be null");

        request.setMerchantId("");
        assertHasViolations("merchantId", validator.validate(request), "Merchant ID must not be empty");
    }
}
