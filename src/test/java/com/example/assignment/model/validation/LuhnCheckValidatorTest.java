package com.example.assignment.model.validation;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class LuhnCheckValidatorTest {

    private final LuhnCheckValidator validator = new LuhnCheckValidator();

    @Test
    void testValidCardNumber() {
        // Visa
        assertTrue(validator.isValid("4532015112830366", null));
        assertTrue(validator.isValid("4111111111111111", null));

        // MasterCard
        assertTrue(validator.isValid("5555555555554444", null));
        assertTrue(validator.isValid("5105105105105100", null));

        // American Express
        assertTrue(validator.isValid("378282246310005", null));
        assertTrue(validator.isValid("371449635398431", null));

        // Discover
        assertTrue(validator.isValid("6011514433546201", null));
        assertTrue(validator.isValid("6011111111111117", null));

        // JCB
        assertTrue(validator.isValid("3530111333300000", null));
        assertTrue(validator.isValid("3566002020360505", null));

        // Maestro
        assertTrue(validator.isValid("6759649826438453", null));
        assertTrue(validator.isValid("6762765696545485", null));
    }

    @Test
    void testInvalidCardNumber() {
        // Visa
        assertFalse(validator.isValid("4532015112830367", null));
        assertFalse(validator.isValid("4485040993287618", null));

        // MasterCard
        assertFalse(validator.isValid("5480457716224144", null));
        assertFalse(validator.isValid("5264684052582487", null));

        // American Express
        assertFalse(validator.isValid("378282246310006", null));
        assertFalse(validator.isValid("371449635398432", null));

        // Discover
        assertFalse(validator.isValid("6011514433546202", null));
        assertFalse(validator.isValid("6011111111111118", null));

        // JCB
        assertFalse(validator.isValid("3530111333300001", null));
        assertFalse(validator.isValid("3566002020360506", null));

        // Maestro
        assertFalse(validator.isValid("6771549495587", null));
        assertFalse(validator.isValid("6762765696545486", null));
    }

    @Test
    void testCardNumberWithSpaces() {
        assertFalse(validator.isValid("4532 0151 1283 0366", null)); // Valid Visa with spaces
        assertFalse(validator.isValid("4532 0151 1283 0367", null)); // Invalid Visa with spaces
    }

    @Test
    void testCardNumberWithDashes() {
        assertFalse(validator.isValid("4532-0151-1283-0366", null)); // Valid Visa with dashes
        assertFalse(validator.isValid("4532-0151-1283-0367", null)); // Invalid Visa with dashes
    }

    @Test
    void testEmptyString() {
        assertFalse(validator.isValid("", null)); // Empty string should be invalid
        assertFalse(validator.isValid("    ", null)); // String with only spaces should be invalid
        assertFalse(validator.isValid(null, null)); // Null should be invalid
    }

    @Test
    void testNonNumericCharacters() {
        assertFalse(validator.isValid("4532a01511283a0366", null)); // Contains non-numeric characters
    }

    @Test
    void testShortCardNumber() {
        assertFalse(validator.isValid("1", null)); // A single digit is not valid
        assertFalse(validator.isValid("12", null)); // Two digits are not valid (even though they pass the Luhn algorithm)
    }
}