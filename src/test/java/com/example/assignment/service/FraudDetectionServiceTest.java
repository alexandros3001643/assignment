package com.example.assignment.service;

import com.example.assignment.model.PaymentRequest;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

@ExtendWith(SpringExtension.class)
@SpringBootTest
public class FraudDetectionServiceTest {

    @Autowired
    private FraudDetectionService fraudDetectionService;

    @Test
    public void testFraudulentTransaction() {
        PaymentRequest fraudulentRequest = new PaymentRequest();
        fraudulentRequest.setAmount(15000.0);
        assertTrue(fraudDetectionService.isFraudulent(fraudulentRequest));
    }

    @Test
    public void testNonFraudulentTransaction() {
        PaymentRequest nonFraudulentRequest = new PaymentRequest();
        nonFraudulentRequest.setAmount(500.0);
        assertFalse(fraudDetectionService.isFraudulent(nonFraudulentRequest));
    }
}