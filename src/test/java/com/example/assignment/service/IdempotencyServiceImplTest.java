package com.example.assignment.service;

import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class IdempotencyServiceImplTest {

    private InMemoryIdempotencyService idempotencyService;

    @BeforeEach
    void setUp() {
        idempotencyService = new InMemoryIdempotencyService();
    }

    @Test
    void whenIdempotencyKeyIsPresent_returnCachedResponse() {
        // Given
        String idempotencyKey = "someKey";
        PaymentResponse expectedResponse = new PaymentResponse(PaymentStatus.APPROVED, "Transaction successful");
        idempotencyService.storeResponse(idempotencyKey, expectedResponse);

        // When
        Optional<PaymentResponse> actualResponse = idempotencyService.getResponse(idempotencyKey);

        // Then
        assertTrue(actualResponse.isPresent());
        assertEquals(expectedResponse, actualResponse.get());
    }

    @Test
    void whenIdempotencyKeyIsNotPresent_returnEmptyOptional() {
        // Given
        String idempotencyKey = "someKey";

        // When
        Optional<PaymentResponse> actualResponse = idempotencyService.getResponse(idempotencyKey);

        // Then
        assertFalse(actualResponse.isPresent());
    }

    @Test
    void storeResponse_shouldStoreTheResponse() {
        // Given
        String idempotencyKey = "someKey";
        PaymentResponse expectedResponse = new PaymentResponse(PaymentStatus.APPROVED, "Transaction successful");

        // When
        idempotencyService.storeResponse(idempotencyKey, expectedResponse);

        // Then
        Optional<PaymentResponse> actualResponse = idempotencyService.getResponse(idempotencyKey);
        assertTrue(actualResponse.isPresent());
        assertEquals(expectedResponse, actualResponse.get());
    }
}
