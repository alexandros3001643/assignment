package com.example.assignment.service;

import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import com.example.assignment.service.psp.PaymentServiceProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class PaymentServiceTest {

    @Mock
    private PspRouter pspRouter;

    @Mock
    private PaymentServiceProvider paymentServiceProvider;

    @Mock
    private FraudDetectionService fraudDetectionService;

    @InjectMocks
    private PaymentService paymentService;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void whenTransactionIsNotFraud_processPayment() {
        // Given
        PaymentRequest paymentRequest = new PaymentRequest();
        PaymentResponse expectedResponse = new PaymentResponse(PaymentStatus.APPROVED, "Transaction successful");
        when(fraudDetectionService.isFraudulent(paymentRequest)).thenReturn(false);
        when(pspRouter.route(paymentRequest)).thenReturn(paymentServiceProvider);
        when(paymentServiceProvider.processPayment(paymentRequest)).thenReturn(expectedResponse);

        // When
        PaymentResponse actualResponse = paymentService.processPayment(paymentRequest);

        // Then
        assertEquals(expectedResponse, actualResponse);
        verify(pspRouter, atLeastOnce()).route(paymentRequest);
    }

    @Test
    void whenTransactionIsFraud_processPayment() {
        // Given
        PaymentRequest paymentRequest = new PaymentRequest();
        PaymentResponse expectedResponse = new PaymentResponse(PaymentStatus.DENIED, "Transaction denied");
        when(fraudDetectionService.isFraudulent(paymentRequest)).thenReturn(true);
        when(paymentServiceProvider.processPayment(paymentRequest)).thenReturn(expectedResponse);

        // When
        PaymentResponse actualResponse = paymentService.processPayment(paymentRequest);

        // Then
        assertEquals(expectedResponse, actualResponse);
        verify(pspRouter, never()).route(paymentRequest);
    }
}
