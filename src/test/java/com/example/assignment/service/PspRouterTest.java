package com.example.assignment.service;

import com.example.assignment.model.Card;
import com.example.assignment.model.PaymentRequest;
import com.example.assignment.service.balancer.MonetaryVolumeBalancer;
import com.example.assignment.service.balancer.RoundRobinBalancer;
import com.example.assignment.service.psp.PaymentServiceProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.eq;
import static org.mockito.Mockito.when;

class PspRouterTest {

    @Spy
    private PaymentServiceProvider psp1Service;

    @Spy
    private PaymentServiceProvider psp2Service;

    @Mock
    private RoundRobinBalancer roundRobinBalancer;

    @Mock
    private MonetaryVolumeBalancer monetaryVolumeBalancer;
    private PspRouter pspRouter;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        when(psp1Service.getIdentifier()).thenReturn("Psp1Service");
        when(psp2Service.getIdentifier()).thenReturn("Psp2Service");

        pspRouter = new PspRouter(psp1Service, roundRobinBalancer, monetaryVolumeBalancer);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void route_shouldReturnPsp1ServiceForVisaAndEur() {
        PaymentRequest request = new PaymentRequest();
        Card card = new Card();
        card.setCardNumber("4111111111111111"); //Visa
        request.setCard(card);
        request.setCurrency("EUR");
        request.setAmount(5.0);

        PaymentServiceProvider result = pspRouter.route(request);
        assertEquals(psp1Service, result);
        assertEquals(5.0, psp1Service.getMonetaryVolume("EUR").doubleValue());
    }

    @Test
    void route_shouldReturnPspBasedOnMonetaryVolumeForMastercardAndUsd() {
        PaymentRequest request = new PaymentRequest();
        Card card = new Card();
        card.setCardNumber("5555555555554444"); //Mastercard
        request.setCard(card);
        request.setCurrency("USD");
        request.setAmount(1.0);

        when(monetaryVolumeBalancer.balance(eq("USD"))).thenReturn(psp2Service);

        PaymentServiceProvider result = pspRouter.route(request);
        assertEquals(psp2Service, result);
        assertEquals(1.0, psp2Service.getMonetaryVolume("USD").doubleValue());
    }

    @Test
    void route_shouldReturnPspBasedOnRoundRobinForOtherCards() {
        PaymentRequest request = new PaymentRequest();
        Card card = new Card();
        card.setCardNumber("341111111111111"); //AMEX
        request.setCard(card);
        request.setCurrency("GBP");
        request.setAmount(2.0);

        when(roundRobinBalancer.balance()).thenReturn(psp1Service);

        PaymentServiceProvider result = pspRouter.route(request);
        assertEquals(psp1Service, result);
        assertEquals(2.0, psp1Service.getMonetaryVolume("GBP").doubleValue());
    }
}
