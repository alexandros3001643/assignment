package com.example.assignment.service.balancer;

import com.example.assignment.service.psp.PaymentServiceProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;
import java.util.concurrent.atomic.DoubleAdder;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;

class MonetaryVolumeBalancerTest {

    @Mock
    private PaymentServiceProvider provider1;

    @Mock
    private PaymentServiceProvider provider2;

    @Mock
    private PaymentServiceProvider provider3;
    private AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
        when(provider1.getIdentifier()).thenReturn("Psp1Service");
        when(provider2.getIdentifier()).thenReturn("Psp2Service");
        when(provider3.getIdentifier()).thenReturn("Psp3Service");
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void balance_shouldSelectProviderWithMinimumMonetaryVolume() {
        when(provider1.getMonetaryVolume("USD")).thenReturn(new DoubleAdder() {{add(100.0);}});
        when(provider2.getMonetaryVolume("USD")).thenReturn(new DoubleAdder() {{add(50.0);}});
        when(provider3.getMonetaryVolume("USD")).thenReturn(new DoubleAdder() {{add(75.0);}});

        List<PaymentServiceProvider> providers = Arrays.asList(provider1, provider2, provider3);
        MonetaryVolumeBalancer balancer = new MonetaryVolumeBalancer(providers);
        PaymentServiceProvider selectedProvider = balancer.balance("USD");
        assertEquals(provider2, selectedProvider);
    }

    @Test
    void balance_shouldThrowExceptionIfNoProvidersAvailable() {
        List<PaymentServiceProvider> emptyProviders = List.of();
        RuntimeException exception = assertThrows(RuntimeException.class, () -> new MonetaryVolumeBalancer(emptyProviders));

        assertEquals("Psp1Service and Psp2Service are required for the monetary volume balancer", exception.getMessage());

        List<PaymentServiceProvider> providers = Arrays.asList(provider2, provider3);
        exception = assertThrows(RuntimeException.class, () -> new MonetaryVolumeBalancer(providers));

        assertEquals("Psp1Service and Psp2Service are required for the monetary volume balancer", exception.getMessage());
    }
}
