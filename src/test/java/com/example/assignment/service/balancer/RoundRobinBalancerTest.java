package com.example.assignment.service.balancer;

import com.example.assignment.service.psp.PaymentServiceProvider;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RoundRobinBalancerTest {

    @Mock
    private PaymentServiceProvider provider1;

    @Mock
    private PaymentServiceProvider provider2;

    @Mock
    private PaymentServiceProvider provider3;

    AutoCloseable closeable;

    @BeforeEach
    void setUp() {
        closeable = MockitoAnnotations.openMocks(this);
    }

    @AfterEach
    void tearDown() throws Exception {
        closeable.close();
    }

    @Test
    void balance_shouldDistributeRequestsInRoundRobinOrder() {
        List<PaymentServiceProvider> providers = Arrays.asList(provider1, provider2, provider3);
        RoundRobinBalancer balancer = new RoundRobinBalancer(providers);

        assertEquals(provider1, balancer.balance());
        assertEquals(provider2, balancer.balance());
        assertEquals(provider3, balancer.balance());
        assertEquals(provider1, balancer.balance());
    }

    @Test
    void balance_shouldHandleSingleProvider() {
        List<PaymentServiceProvider> singleProvider = List.of(provider1);
        RoundRobinBalancer balancer = new RoundRobinBalancer(singleProvider);

        assertEquals(provider1, balancer.balance());
        assertEquals(provider1, balancer.balance());
    }
}
