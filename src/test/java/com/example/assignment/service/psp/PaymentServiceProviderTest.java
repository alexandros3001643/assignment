package com.example.assignment.service.psp;

import com.example.assignment.model.Card;
import com.example.assignment.model.PaymentRequest;
import com.example.assignment.model.PaymentResponse;
import com.example.assignment.model.PaymentStatus;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PaymentServiceProviderTest {

    private PaymentRequest createPaymentRequest(String cardNumber) {
        PaymentRequest request = new PaymentRequest();
        Card card = new Card();
        card.setCardNumber(cardNumber);  // Valid Visa card number
        card.setExpiryDate("12/99");  // Valid future date
        card.setCvv("123");  // Valid CVV
        request.setCard(card);
        request.setAmount(100.0);
        request.setCurrency("USD");
        request.setMerchantId("MERCHANT123");
        return request;
    }

    @Test
    void processPayment_shouldApproveForEvenLastDigitCardNumber() {
        PaymentServiceProvider provider = new PaymentServiceProvider() {}; // Creating an anonymous subclass for testing
        PaymentRequest request = createPaymentRequest("4111111111111110");

        PaymentResponse response = provider.processPayment(request);

        assertEquals(PaymentStatus.APPROVED, response.getStatus());
        assertEquals("Transaction successful", response.getMessage());
        assertEquals(0.0, provider.getMonetaryVolume("USD").doubleValue());
    }

    @Test
    void processPayment_shouldDenyForOddLastDigitCardNumber() {
        PaymentServiceProvider provider = new PaymentServiceProvider() {}; // Creating an anonymous subclass for testing
        PaymentRequest request = createPaymentRequest("4111111111111111");

        PaymentResponse response = provider.processPayment(request);

        assertEquals(PaymentStatus.DENIED, response.getStatus());
        assertEquals("Transaction denied", response.getMessage());
        assertEquals(-100.0, provider.getMonetaryVolume("USD").doubleValue());
    }

    @Test
    void getMonetaryVolume_shouldReturnZeroForNonexistentCurrency() {
        PaymentServiceProvider provider = new PaymentServiceProvider() {}; // Creating an anonymous subclass for testing
        double volume = provider.getMonetaryVolume("EUR").doubleValue();

        assertEquals(0.0, volume);
    }
}
